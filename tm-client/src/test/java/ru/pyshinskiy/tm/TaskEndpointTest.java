package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pyshinskiy.tm.api.endpoint.*;
import ru.pyshinskiy.tm.endpoint.ProjectEndpointService;
import ru.pyshinskiy.tm.endpoint.SessionEndpointService;
import ru.pyshinskiy.tm.endpoint.TaskEndpointService;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;

@Category(ru.pyshinskiy.tm.TaskEndpointTestIntegrate.class)
public class TaskEndpointTest extends Assert {

    @Nullable
    private ISessionEndpoint sessionEndpoint;

    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Nullable
    private SessionDTO sessionDTOUser;

    @Nullable
    private SessionDTO sessionDTOAdmin;

    @Before
    public void setUp() throws Exception {
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
        taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        sessionDTOUser = sessionEndpoint.createSession("test", "1234");
        sessionDTOAdmin = sessionEndpoint.createSession("admin", "1234");
    }

    @After
    public void tearDown() throws Exception {
        taskEndpoint.removeAllTasksByUserId(sessionDTOAdmin);
        taskEndpoint.removeAllTasksByUserId(sessionDTOUser);
        projectEndpoint.removeAllProjectsByUserId(sessionDTOUser);
        sessionEndpoint.removeSession(sessionDTOUser.getUserId(), sessionDTOUser.getId());
        sessionEndpoint.removeSession(sessionDTOAdmin.getUserId(), sessionDTOAdmin.getId());
    }

    @Test
    public void findOneTaskByUserId() throws Exception {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskEndpoint.persistTask(sessionDTOUser, taskDTO);
        assertEquals(taskDTO.getId(), taskEndpoint.findOneTaskByUserId(sessionDTOUser, taskDTO.getId()).getId());
    }

    @Test
    public void findAllTasksByUserId() throws Exception {
        @NotNull final List<TaskDTO> userTasks = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            @NotNull final TaskDTO taskDTO = createTaskDTO();
            userTasks.add(taskDTO);
            taskEndpoint.persistTask(sessionDTOUser, taskDTO);
        }
        assertEquals(userTasks.size(), taskEndpoint.findAllTasksByUserId(sessionDTOUser).size());
    }

    @Test
    public void persistTask() throws Exception {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskEndpoint.persistTask(sessionDTOUser, taskDTO);
        assertEquals(taskDTO.getId(), taskEndpoint.findOneTaskByUserId(sessionDTOUser, taskDTO.getId()).getId());
    }

    @Test
    public void mergeTask() throws Exception {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskDTO.setName("MERGED PROJECT TEST 1");
        taskEndpoint.mergeTask(sessionDTOUser, taskDTO);
        assertEquals(taskDTO.getName(), taskEndpoint.findOneTaskByUserId(sessionDTOUser, taskDTO.getId()).getName());
    }

    @Test
    public void removeTaskByUserId() throws Exception {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskEndpoint.removeTaskByUserId(sessionDTOUser, taskDTO.getId());
        assertNull(taskEndpoint.findOneTaskByUserId(sessionDTOUser, taskDTO.getId()));
    }

    @Test
    public void removeAllTasksByUserId() throws Exception {
        for(int i = 0; i < 5; i++) {
            taskEndpoint.persistTask(sessionDTOUser, createTaskDTO());
        }
        taskEndpoint.removeAllTasksByUserId(sessionDTOUser);
        assertTrue(taskEndpoint.findAllTasksByUserId(sessionDTOUser).isEmpty());
    }

    @Test
    public void findTasksByName() throws Exception {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskEndpoint.persistTask(sessionDTOUser, taskDTO);
        assertEquals(taskDTO.getName(), taskEndpoint.findTasksByName(sessionDTOUser, taskDTO.getName()).get(0).getName());
    }

    @Test
    public void findTasksByDescription() throws Exception {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskDTO.setDescription("FIND BY DESCRIPTION");
        taskEndpoint.persistTask(sessionDTOUser, taskDTO);
        assertEquals(taskDTO.getDescription(), taskEndpoint.findTasksByDescription(sessionDTOUser, taskDTO.getDescription()).get(0).getDescription());
    }

    @Test
    public void findOneTask() throws Exception {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskEndpoint.persistTask(sessionDTOAdmin, taskDTO);
        assertEquals(taskDTO.getId(), taskEndpoint.findOneTask(sessionDTOAdmin, taskDTO.getId()).getId());
    }

  /*  @Test
    public void findAllTasks() throws Exception {
        taskEndpoint.removeAllTasks(sessionDTOAdmin);
        @NotNull final List<TaskDTO> allTasks = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            @NotNull final TaskDTO taskDTO = createTaskDTO();
            allTasks.add(taskDTO);
            taskEndpoint.persistTask(sessionDTOUser, taskDTO);
        }
        for(int i = 0; i < 5; i++) {
            @NotNull final TaskDTO taskDTO = createTaskDTO();
            allTasks.add(taskDTO);
            taskEndpoint.persistTask(sessionDTOAdmin, taskDTO);
        }
        assertEquals(allTasks.size(), taskEndpoint.findAllTasks(sessionDTOUser).size());
    }*/

    @Test
    public void removeTask() throws Exception {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskEndpoint.persistTask(sessionDTOAdmin, taskDTO);
        taskEndpoint.removeTask(sessionDTOAdmin, taskDTO.getId());
        assertNull(taskEndpoint.findOneTask(sessionDTOAdmin, taskDTO.getId()));
    }

    /*@Test
    public void removeAllTasks() throws Exception {
        for(int i = 0; i < 5; i++) {
            taskEndpoint.persistTask(sessionDTOUser, createTaskDTO());
        }
        for(int i = 0; i < 5; i++) {
            taskEndpoint.persistTask(sessionDTOAdmin, createTaskDTO());
        }
        taskEndpoint.removeAllTasks(sessionDTOAdmin);
        assertTrue(taskEndpoint.findAllTasks(sessionDTOAdmin).isEmpty());
    }*/

    @Test
    public void findTasksByProjectId() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(sessionDTOUser.getUserId());
        projectEndpoint.persistProject(sessionDTOUser, projectDTO);
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            @NotNull final TaskDTO taskDTO = createTaskDTO();
            taskDTO.setProjectId(projectDTO.getId());
            taskEndpoint.persistTask(sessionDTOUser, taskDTO);
            tasks.add(taskDTO);
        }
        assertEquals(tasks.size(), taskEndpoint.findTasksByProjectId(sessionDTOUser, projectDTO.getId()).size());
    }

    @NotNull
    private TaskDTO createTaskDTO() throws Exception{
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(sessionDTOUser.getUserId());
        taskDTO.setName("TASK TEST " + new Random().toString());
        taskDTO.setDescription("TEST DESCRIPTION 2");
        taskDTO.setStatus(Status.DONE);
        taskDTO.setStartDate(toXMLGregorianCalendar(parseDateFromString("11.12.2019")));
        taskDTO.setFinishDate(toXMLGregorianCalendar(parseDateFromString("12.12.2019")));
        return taskDTO;
    }
}
