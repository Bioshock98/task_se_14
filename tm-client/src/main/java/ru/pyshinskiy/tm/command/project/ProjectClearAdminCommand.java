package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class ProjectClearAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSessionDTO() == null) return false;
        return bootstrap.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_clear_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all users projects";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        projectEndpoint.removeAllProjects(bootstrap.getSessionDTO());
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
