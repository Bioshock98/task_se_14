package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class TaskClearAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSessionDTO() == null) return false;
        return bootstrap.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_clear_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "clear all users tasks";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getTaskEndpoint().removeAllTasks(bootstrap.getSessionDTO());
    }
}
