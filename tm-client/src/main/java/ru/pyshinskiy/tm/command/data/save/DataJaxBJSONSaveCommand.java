package ru.pyshinskiy.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataJaxBJSONSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_jaxB_json_save";
    }

    @Override
    @NotNull
    public String description() {
        return "save data by jaxB in json format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA JAXB JSON SAVE");
        //bootstrap.getUserEndpoint().saveDataJsonByJaxB(bootstrap.getSession());
        System.out.println("[OK]");
    }
}
