package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Status;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskChangeStatusCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_change_status";
    }

    @Override
    @NotNull
    public String description() {
        return "change task status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CHANGE STATUS]");
        System.out.println("ENTER TASK ID");
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(bootstrap.getSessionDTO());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskByUserId(bootstrap.getSessionDTO(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        System.out.println("Current task status is " + task.getStatus());
        System.out.println("ENTER NEW STATUS");
        @NotNull final Status newStatus = Status.valueOf(bootstrap.getTerminalService().nextLine());
        task.setStatus(newStatus);
        taskEndpoint.mergeTask(bootstrap.getSessionDTO(), task);
        System.out.println("[OK]");
    }
}
