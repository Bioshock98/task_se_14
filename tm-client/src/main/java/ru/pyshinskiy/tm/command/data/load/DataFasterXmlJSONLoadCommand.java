package ru.pyshinskiy.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataFasterXmlJSONLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_fasterXml_json_load";
    }

    @Override
    @NotNull
    public String description() {
        return "load data by fasterXml from json format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA FASTERXML JSON LOAD");
        //bootstrap.getUserEndpoint().loadDataJsonByFasterXml(bootstrap.getSessionDTO());
        System.out.println("[OK]");
    }
}
