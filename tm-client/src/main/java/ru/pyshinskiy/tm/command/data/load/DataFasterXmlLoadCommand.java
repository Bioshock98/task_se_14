package ru.pyshinskiy.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataFasterXmlLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_fasterXml_load";
    }

    @Override
    public @NotNull String description() {
        return "load data by fasterXml from xml format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA FASTERXML XML LOAD");
        //bootstrap.getUserEndpoint().loadDataXmlByFasterXml(bootstrap.getSessionDTO());
        System.out.println("[OK]");
    }
}
