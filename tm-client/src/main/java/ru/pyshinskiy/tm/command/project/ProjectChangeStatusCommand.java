package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.api.endpoint.Status;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectChangeStatusCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_change_status";
    }

    @Override
    @NotNull
    public String description() {
        return "change project status";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        System.out.println("[PROJECT CHANGE STATUS]");
        System.out.println("ENTER PROJECT ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(bootstrap.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        @Nullable final ProjectDTO project = projectEndpoint.findOneProjectByUserId(bootstrap.getSessionDTO(), projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        System.out.println("Current project status is " + project.getStatus());
        System.out.println("ENTER NEW STATUS");
        @NotNull final Status newStatus = Status.valueOf(bootstrap.getTerminalService().nextLine());
        project.setStatus(newStatus);
        bootstrap.getProjectEndpoint().mergeProject(bootstrap.getSessionDTO(), project);
        System.out.println("[OK]");
    }
}
