package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = bootstrap.getTerminalService().nextLine();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        @NotNull final SessionDTO sessionDTO = bootstrap.getSessionDTO();
        switch (option) {
            case "createTime" :
                printTasks(taskEndpoint.sortTasksByCreateTime(bootstrap.getSessionDTO(), sessionDTO.getUserId(), 1));
                break;
            case "startDate" :
                printTasks(taskEndpoint.sortTasksByStartDate(bootstrap.getSessionDTO(), sessionDTO.getUserId(), 1));
                break;
            case "finishDate" :
                printTasks(taskEndpoint.sortTasksByFinishDate(bootstrap.getSessionDTO(), sessionDTO.getUserId(), 1));
                break;
            case "status" :
                printTasks(taskEndpoint.sortTasksByStatus(bootstrap.getSessionDTO(), sessionDTO.getUserId(), 1));
        }
        System.out.println("[OK]");
    }
}
