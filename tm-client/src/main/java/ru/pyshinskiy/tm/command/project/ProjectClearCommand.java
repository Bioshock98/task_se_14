package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_clear";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all projects";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        projectEndpoint.removeAllProjectsByUserId(bootstrap.getSessionDTO());
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
