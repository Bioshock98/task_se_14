package ru.pyshinskiy.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataFasterXmlSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_fasterXml_save";
    }

    @Override
    @NotNull
    public String description() {
        return "save data by fasterXml in xml format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA FASTERXML XML SAVE");
        //bootstrap.getUserEndpoint().saveDataXmlByFasterXml(bootstrap.getSession());
        System.out.println("[OK]");
    }
}
