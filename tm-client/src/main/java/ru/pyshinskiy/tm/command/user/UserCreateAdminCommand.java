package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class UserCreateAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSessionDTO() == null) return false;
        return bootstrap.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "user_create_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "create a new user or administrator";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER ROLE");
        @NotNull final Role role =  Role.valueOf(terminalService.nextLine().toUpperCase());
        System.out.println("ENTER USERNAME");
        @NotNull final String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = terminalService.nextLine();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(role);
        bootstrap.getUserEndpoint().persistUser(user);
        System.out.println("[OK]");
    }
}
