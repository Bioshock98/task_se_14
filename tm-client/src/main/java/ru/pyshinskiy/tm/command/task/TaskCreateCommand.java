package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_create";
    }

    @Override
    @NotNull
    public String description() {
        return "create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER TASK NAME]");
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(bootstrap.getSessionDTO().getUserId());
        task.setName(terminalService.nextLine());
        System.out.println("ENTER TASK DESCRIPTION");
        task.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER END DATE");
        task.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        bootstrap.getTaskEndpoint().persistTask(bootstrap.getSessionDTO(), task);
        System.out.println("[OK]");
    }
}
