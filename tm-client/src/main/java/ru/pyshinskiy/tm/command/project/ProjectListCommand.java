package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        System.out.println("[PROJECT LIST]");
        System.out.println("[DO YOU WANT TO SORT RESULT?]");
        System.out.println("[ENTER \'y\' or \'n\']");
        @NotNull final String doSort = bootstrap.getTerminalService().nextLine();
        if("y".equals(doSort)) {
            System.out.println("CHOOSE SORT TYPE");
            System.out.println("[createTime, startDate, finishDate, status]");
            @NotNull final String option = bootstrap.getTerminalService().nextLine();
            @NotNull final SessionDTO session = bootstrap.getSessionDTO();
            switch (option) {
                case "createTime" :
                    printProjects(projectEndpoint.sortProjectsByCreateTime(bootstrap.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "startDate" :
                    printProjects(projectEndpoint.sortProjectsByStartDate(bootstrap.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "finishDate" :
                    printProjects(projectEndpoint.sortProjectsByFinishDate(bootstrap.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "status" :
                    printProjects(projectEndpoint.sortProjectsByStatus(bootstrap.getSessionDTO(), session.getUserId(), 1));
            }
            System.out.println("[OK]");
        }
        else {
            printProjects(projectEndpoint.findAllProjectsByUserId(bootstrap.getSessionDTO()));
            System.out.println("[OK]");
        }
    }
}
