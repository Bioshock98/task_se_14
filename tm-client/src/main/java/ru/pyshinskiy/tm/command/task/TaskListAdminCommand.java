package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSessionDTO() == null) return false;
        return bootstrap.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST ADMIN]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = bootstrap.getTerminalService().nextLine();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        @NotNull final SessionDTO session = bootstrap.getSessionDTO();
        switch (option) {
            case "createTime" :
                printTasks(taskEndpoint.sortTasksByCreateTime(bootstrap.getSessionDTO(), session.getUserId(), 1));
                break;
            case "startDate" :
                printTasks(taskEndpoint.sortTasksByStartDate(bootstrap.getSessionDTO(), session.getUserId(), 1));
                break;
            case "finishDate" :
                printTasks(taskEndpoint.sortTasksByFinishDate(bootstrap.getSessionDTO(), session.getUserId(), 1));
                break;
            case "status" :
                printTasks(taskEndpoint.sortTasksByStatus(bootstrap.getSessionDTO(), session.getUserId(), 1));
        }
        System.out.println("[OK]");
    }
}
