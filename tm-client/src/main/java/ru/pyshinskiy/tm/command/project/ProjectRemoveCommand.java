package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove project and related tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(bootstrap.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        projectEndpoint.removeProjectByUserId(bootstrap.getSessionDTO(), projectId);
        System.out.println("[PROJECT REMOVED]");
    }
}
