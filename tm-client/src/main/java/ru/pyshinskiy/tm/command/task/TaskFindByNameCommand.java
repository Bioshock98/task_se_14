package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskFindByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_find_by_name";
    }

    @Override
    @NotNull
    public String description() {
        return "find task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK FIND BY NAME");
        System.out.println("ENTER TASK NAME");
        @NotNull final String name = bootstrap.getTerminalService().nextLine();
        printTasks(bootstrap.getTaskEndpoint().findTasksByName(bootstrap.getSessionDTO(), name));
        System.out.println("[OK]");
    }
}
