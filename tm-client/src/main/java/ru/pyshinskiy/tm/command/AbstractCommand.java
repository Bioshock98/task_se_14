package ru.pyshinskiy.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.service.TerminalService;

public abstract class AbstractCommand {

    @NotNull
    protected TerminalService terminalService;

    @NotNull
    protected Bootstrap bootstrap;

    public void setBootstrap(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.terminalService = bootstrap.getTerminalService();
    }

    public boolean isAllowed() {
        return bootstrap.getSessionDTO() != null;
    }

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;
}
