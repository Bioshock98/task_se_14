package ru.pyshinskiy.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataJaxBJSONLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_jaxB_json_load";
    }

    @Override
    @NotNull
    public String description() {
        return "load data by jaxB from json format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA JAXB JSON LOAD");
        //bootstrap.getUserEndpoint().loadDataJsonByJaxB(bootstrap.getSessionDTO());
        System.out.println("[OK]");
    }
}
