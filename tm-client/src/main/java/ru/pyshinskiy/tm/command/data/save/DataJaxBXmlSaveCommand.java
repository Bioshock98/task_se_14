package ru.pyshinskiy.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class DataJaxBXmlSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_jaxB_xml_save";
    }

    @Override
    @NotNull
    public String description() {
        return "save data by jaxB in xml format";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DATA JAXB XML SAVE");
        //bootstrap.getUserEndpoint().saveDataXmlByJaxB(bootstrap.getSession());
        System.out.println("[OK]");
    }
}
