package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSessionDTO() == null) return false;
        return bootstrap.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("DO YOU WANT TO SORT PROJECTS?");
        @NotNull final String doSort = bootstrap.getTerminalService().nextLine();
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        @NotNull final SessionDTO session = bootstrap.getSessionDTO();
        if("y".equals(doSort) || "yes".equals(doSort)) {
            System.out.println("CHOOSE SORT TYPE");
            System.out.println("[createTime, startDate, finishDate, status]");
            @NotNull final String option = bootstrap.getTerminalService().nextLine();
            switch (option) {
                case "createTime" :
                    printProjects(projectEndpoint.sortProjectsByCreateTime(bootstrap.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "startDate" :
                    printProjects(projectEndpoint.sortProjectsByStartDate(bootstrap.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "finishDate" :
                    printProjects(projectEndpoint.sortProjectsByFinishDate(bootstrap.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "status" :
                    printProjects(projectEndpoint.sortProjectsByStatus(bootstrap.getSessionDTO(), session.getUserId(), 1));
            }
            System.out.println("[OK]");
        }
        else {
            printProjects(projectEndpoint.findAllProjects(bootstrap.getSessionDTO()));
            System.out.println("[OK]");
        }
    }
}
