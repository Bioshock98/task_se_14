package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_create";
    }

    @Override
    @NotNull
    public String description() {
        return "create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(bootstrap.getSessionDTO().getUserId());
        project.setName(terminalService.nextLine());
        System.out.println("ENTER PROJECT DESCRIPTION");
        project.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER END DATE");
        project.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        bootstrap.getProjectEndpoint().persistProject(bootstrap.getSessionDTO(), project);
        System.out.println("[OK]");
    }
}
