package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectFindByDescriptionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_find_by_description";
    }

    @Override
    @NotNull
    public String description() {
        return "find project by its description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND BY DESCRIPTION]");
        System.out.println("ENTER PROJECT DESCRIPTION");
        @NotNull final String description = bootstrap.getTerminalService().nextLine();
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        printProjects(projectEndpoint.findProjectByDescription(bootstrap.getSessionDTO(), description));
        System.out.println("[OK]");
    }
}
