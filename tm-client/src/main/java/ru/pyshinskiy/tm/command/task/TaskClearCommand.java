package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_clear";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASK]");
        bootstrap.getTaskEndpoint().removeAllTasksByUserId(bootstrap.getSessionDTO());
        System.out.println("[ALL TASKS REMOVED]");
    }
}
