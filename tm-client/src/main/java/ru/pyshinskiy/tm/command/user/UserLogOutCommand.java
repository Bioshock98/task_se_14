package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class UserLogOutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user_log_out";
    }

    @Override
    @NotNull
    public String description() {
        return "log out";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER LOG OUT");
        @Nullable final SessionDTO currentSession = bootstrap.getSessionDTO();
        bootstrap.getSessionEndpoint().removeSession(currentSession.getUserId(), currentSession.getId());
        bootstrap.setSessionDTO(null);
        System.out.println("[OK]");
    }
}
