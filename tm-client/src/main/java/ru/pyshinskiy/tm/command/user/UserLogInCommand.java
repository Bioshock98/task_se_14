package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "user_log_in";
    }

    @Override
    @NotNull
    public String description() {
        return "log in";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOG IN]");
        System.out.println("ENTER LOGIN");
        @NotNull String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull String password = terminalService.nextLine();
        @Nullable SessionDTO session = bootstrap.getSessionEndpoint().createSession(login, password);
        while(session == null) {
            System.out.println("Invalid login or password");
            System.out.println("ENTER LOGIN");
            login = terminalService.nextLine();
            System.out.println("ENTER PASSWORD");
            password = terminalService.nextLine();
            session = bootstrap.getSessionEndpoint().createSession(login, password);
        }
        @Nullable final SessionDTO currentSession = bootstrap.getSessionDTO();
        if(currentSession != null) bootstrap.getSessionEndpoint().removeSession(currentSession.getUserId(), currentSession.getId());
        bootstrap.setSessionDTO(session);
        bootstrap.startSessionUpdater();
        System.out.println("[OK]");
    }
}
