package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListByProjectCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_list_by_project";
    }

    @Override
    @NotNull
    public String description() {
        return "show tasks by project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("[ENTER PROJECT ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjects(bootstrap.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        @NotNull final List<TaskDTO> tasksByProjectId = taskEndpoint.findTasksByProjectId(bootstrap.getSessionDTO(), projectId);
        printTasks(tasksByProjectId);
        System.out.println("[OK]");
    }
}
