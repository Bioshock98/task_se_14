package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskFindByDescriptionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_find_by_description";
    }

    @Override
    @NotNull
    public String description() {
        return "find task by description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK FIND BY DESCRIPTION]");
        System.out.println("ENTER TASK DESCRIPTION");
        @NotNull final String description = bootstrap.getTerminalService().nextLine();
        printTasks(bootstrap.getTaskEndpoint().findTasksByDescription(bootstrap.getSessionDTO(), description));
        System.out.println("[OK]");
    }
}
