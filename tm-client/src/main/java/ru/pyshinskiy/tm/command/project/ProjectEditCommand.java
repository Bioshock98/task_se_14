package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_edit";
    }

    @Override
    @NotNull
    public String description() {
        return "edit existing project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(bootstrap.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        @Nullable final ProjectDTO project = projectEndpoint.findOneProject(bootstrap.getSessionDTO(), projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        @NotNull final ProjectDTO anotherProject = new ProjectDTO();
        anotherProject.setUserId(bootstrap.getSessionDTO().getUserId());
        System.out.println("ENTER NAME");
        anotherProject.setName(terminalService.nextLine());
        anotherProject.setId(project.getId());
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherProject.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER FINISH DATE");
        anotherProject.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        projectEndpoint.mergeProject(bootstrap.getSessionDTO(), anotherProject);
        System.out.println("[OK]");
    }
}
