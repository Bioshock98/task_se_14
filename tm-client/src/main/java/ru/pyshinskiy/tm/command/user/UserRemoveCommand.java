package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUsers;

public final class UserRemoveCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSessionDTO() == null) return false;
        return bootstrap.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "user_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove existing user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER ID");
        @NotNull final IUserEndpoint userEndpoint = bootstrap.getUserEndpoint();
        @NotNull final List<UserDTO> users = userEndpoint.findAllUsers(bootstrap.getSessionDTO());
        printUsers(users);
        final int userNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String userId = users.get(userNumber).getId();
        bootstrap.getUserEndpoint().removeUser(bootstrap.getSessionDTO(), userId);
        System.out.println("USER REMOVED");
    }
}
