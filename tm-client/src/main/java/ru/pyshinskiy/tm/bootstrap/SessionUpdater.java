package ru.pyshinskiy.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.Exception_Exception;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;

public final class SessionUpdater extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public SessionUpdater(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void run() {
        while(true) {
            if(bootstrap.getSessionDTO() != null) {
                try {
                    Thread.sleep(150000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                @Nullable SessionDTO updatedSession = null;
                try {
                    updatedSession = bootstrap.getSessionEndpoint().updateSession(bootstrap.getSessionDTO());
                    bootstrap.setSessionDTO(updatedSession);
                    System.out.println("[Session update]");
                }
                catch (Exception_Exception e) {
                    System.out.println("Updating session is failed");
                }
            }
        }
    }
}
