package ru.pyshinskiy.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.pyshinskiy.tm.api.endpoint.*;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.ProjectEndpointService;
import ru.pyshinskiy.tm.endpoint.SessionEndpointService;
import ru.pyshinskiy.tm.endpoint.TaskEndpointService;
import ru.pyshinskiy.tm.endpoint.UserEndpointService;
import ru.pyshinskiy.tm.service.TerminalService;

import java.lang.Exception;
import java.util.*;

public class Bootstrap {

    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable private SessionDTO sessionDTO;

    @NotNull private final TerminalService terminalService = new TerminalService();

    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull private final Map<String, AbstractCommand> commands = new TreeMap<>();

    public Bootstrap() throws Exception_Exception {
    }

    @NotNull
    public ISessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Nullable
    public SessionDTO getSessionDTO() {
        return sessionDTO;
    }

    public void setSessionDTO(@Nullable final SessionDTO sessionDTO) {
        this.sessionDTO = sessionDTO;
    }

    @NotNull
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new LinkedList<>(commands.values());
    }

    public void startSessionUpdater() {
        @NotNull final SessionUpdater sessionUpdater = new SessionUpdater(this);
        sessionUpdater.setDaemon(true);
        sessionUpdater.start();
    }

    public void start() throws Exception {
        init();
        System.out.println("***WELCOME TO TASK MANAGER***");
        @Nullable String command;
        while(true) {
            command = terminalService.nextLine();
            try {
                execute(command);
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    private void init() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.pyshinskiy.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(ru.pyshinskiy.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz.newInstance());
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if(command == null || command.isEmpty()) return;
        if("exit".equals(command)) {
            if(sessionDTO != null) sessionEndpoint.removeSession(sessionDTO.getUserId(), sessionDTO.getId());
            System.exit(0);
        }
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if(abstractCommand == null) {
            System.out.println("UNKNOW COMMAND");
            return;
        }
        if(!abstractCommand.isAllowed()) {
            System.out.println("!UNAVAILABLE COMMAND!");
            return;
        }
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) throws Exception {
        @Nullable final String cliCommand = command.command();
        @Nullable final String cliDescription = command.description();
        if (cliCommand.isEmpty())
            throw new Exception();
        if (cliDescription.isEmpty())
            throw new Exception();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }
}
