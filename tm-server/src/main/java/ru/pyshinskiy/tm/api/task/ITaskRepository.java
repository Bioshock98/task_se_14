package ru.pyshinskiy.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    @NotNull
    List<Task> findAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @Nullable
    Task findOne(@NotNull final String id) throws Exception;

    void persist(@NotNull final Task task) throws Exception;

    void merge(@NotNull final Task task) throws Exception;

    void remove(@NotNull final Task task) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    Task findOneByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    @NotNull
    List<Task> findByName(@NotNull final String userId, @NotNull final String name) throws Exception;

    @NotNull
    List<Task> findByDescription(@NotNull final String userId, @NotNull final String description) throws Exception;

    void removeByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    List<Task> sortByCreateTime(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<Task> sortByStartDate(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<Task> sortByFinishDate(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<Task> sortByStatus(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception;
}
