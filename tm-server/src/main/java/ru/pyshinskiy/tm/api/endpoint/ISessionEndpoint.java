package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    @Nullable
    SessionDTO findSession(@NotNull final String id) throws Exception;

    @WebMethod
    @Nullable
    SessionDTO createSession(@NotNull final String login, @NotNull final String password) throws Exception;

    @WebMethod
    @Nullable
    SessionDTO updateSession(@NotNull final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void removeSession(@Nullable final String userId, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllSessions() throws Exception;

    @WebMethod
    void removeAllSessionsByUserId(@Nullable final String userId) throws Exception;
}
