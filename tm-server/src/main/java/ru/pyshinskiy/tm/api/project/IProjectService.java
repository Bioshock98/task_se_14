package ru.pyshinskiy.tm.api.project;

import ru.pyshinskiy.tm.api.wbs.IAbstractVBSService;
import ru.pyshinskiy.tm.entity.Project;

public interface IProjectService extends IAbstractVBSService<Project> {
}
