package ru.pyshinskiy.tm.api.wbs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;

import java.util.List;

public interface IAbstractVBSService<T> extends IService<T> {

    @NotNull
    List<T> findAllByUserId(@Nullable final String userId) throws Exception;

    @Nullable
    T findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    List<T> findByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    @Nullable
    List<T> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception;

    void removeByUserId(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    @NotNull
    List<T> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception;

    @NotNull
    List<T> sortByStartDate(@Nullable final String userId, final int direction) throws Exception;

    @NotNull
    List<T> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception;

    @NotNull
    List<T> sortByStatus(@Nullable final String userId, final int direction) throws Exception;
}
