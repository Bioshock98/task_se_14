package ru.pyshinskiy.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;

import java.util.List;

public interface
IProjectRepository {

    @NotNull
    List<Project> findAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    List<Project> findAll() throws Exception;

    @Nullable
    Project findOne(@NotNull final String id) throws Exception;

    void persist(@NotNull final Project project) throws Exception;

    void merge(@NotNull final Project project) throws Exception;

    void remove(@NotNull final Project project) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    Project findOneByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    @NotNull
    List<Project> findByName(@NotNull final String userId, @NotNull final String name) throws Exception;

    @NotNull
    List<Project> findByDescription(@NotNull final String userId, @NotNull final String description) throws Exception;

    void removeByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    List<Project> sortByCreateTime(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<Project> sortByStartDate(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<Project> sortByFinishDate(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<Project> sortByStatus(@NotNull final String userId, final int direction) throws Exception;
}
