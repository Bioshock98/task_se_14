package ru.pyshinskiy.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User findOne(@NotNull final String id) throws Exception;

    void persist(@NotNull final User user) throws Exception;

    void merge(@NotNull final User user) throws Exception;

    void remove(@NotNull final User user) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User getUserByLogin(@NotNull final String login) throws Exception;
}
