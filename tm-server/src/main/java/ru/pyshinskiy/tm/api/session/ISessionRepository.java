package ru.pyshinskiy.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    @NotNull
    List<Session> findAll() throws Exception;

    @NotNull
    List<Session> findAllByUserId(@NotNull final String userId) throws Exception;

    @Nullable
    Session findOne(@NotNull final String id) throws Exception;

    void persist(@NotNull final Session session) throws Exception;

    void merge(@NotNull final Session session) throws Exception;

    void remove(@NotNull final Session session) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    Session findOneByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAllByUserId(@NotNull final String userId) throws Exception;
}
