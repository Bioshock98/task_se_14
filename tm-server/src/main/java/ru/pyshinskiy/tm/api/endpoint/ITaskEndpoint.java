package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    @Nullable
    TaskDTO findOneTask(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception;

    @WebMethod
    @NotNull
    List<TaskDTO> findAllTasks(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void persistTask(@Nullable final SessionDTO sessionDTO, @Nullable final TaskDTO taskDTO) throws Exception;

    @WebMethod
    void mergeTask(@Nullable final SessionDTO sessionDTO, @Nullable final TaskDTO taskDTO) throws Exception;

    @WebMethod
    void removeTask(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllTasks(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    @NotNull
    List<TaskDTO> findAllTasksByUserId(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    @Nullable
    TaskDTO findOneTaskByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception;

    @WebMethod
    @Nullable
    List<TaskDTO> findTasksByName(@Nullable final SessionDTO sessionDTO, @Nullable final String name) throws Exception;

    @WebMethod
    @Nullable
    List<TaskDTO> findTasksByDescription(@Nullable final SessionDTO sessionDTO, @Nullable final String description) throws Exception;

    @WebMethod
    void removeTaskByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllTasksByUserId(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    @NotNull
    List<TaskDTO> sortTasksByCreateTime(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<TaskDTO> sortTasksByStartDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<TaskDTO> sortTasksByFinishDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<TaskDTO> sortTasksByStatus(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<TaskDTO> findTasksByProjectId(@Nullable final SessionDTO sessionDTO, @Nullable final String projectId) throws Exception;
}
