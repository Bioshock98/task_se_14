package ru.pyshinskiy.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.Session;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session findOneByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    List<Session> findAllByUserId(@NotNull final String userId) throws Exception;

    void removeByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAllByUserId(@Nullable final String userId) throws Exception;
}
