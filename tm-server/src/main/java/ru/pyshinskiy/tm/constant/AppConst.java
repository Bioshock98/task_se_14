package ru.pyshinskiy.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class AppConst {

    @NotNull public final static String SALT = "ksdnfnfiew";
    @NotNull public final static Integer CICLE = 64;
    @NotNull public final static Integer SESSION_LIFE_TIME = 180000;
}
