package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.repository.SessionRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

public final class SessionService extends AbstractService<Session>  implements ISessionService {

    @NotNull private final Bootstrap bootstrap;

    public SessionService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    @Nullable
    public Session findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception{
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final Session session;
        try {
            session = new SessionRepository(em).findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return session;
    }

    @Override
    public void removeByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new SessionRepository(em).removeByUserId(userId, id);
        em.getTransaction().commit();
        em.close();
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid session id");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final Session session;
        try {
            session = new SessionRepository(em).findOne(id);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return session;
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Session> sessions = new SessionRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return sessions;
    }

    @Override
    @NotNull
    public List<Session> findAllByUserId(@NotNull final String userId) {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Session> sessions = new SessionRepository(em).findAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
        return sessions;
    }

    @Override
    public void persist(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new SessionRepository(em).persist(session);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void merge(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new SessionRepository(em).merge(session);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid session id");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new SessionRepository(em).remove(findOne(id));
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new SessionRepository(em).removeAll();
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("userId is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new SessionRepository(em).removeAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
    }
}
