package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.util.List;

public final class ProjectService extends AbstractWBSService<Project> implements IProjectService {

    @NotNull private final Bootstrap bootstrap;

    public ProjectService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Project findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) return null;
        if(userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final Project project;
        try {
            project = new ProjectRepository(em).findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return project;
    }

    @Override
    @NotNull
    public List<Project> findAllByUserId(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Project> projects = new ProjectRepository(em).findAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Override
    public void removeByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new ProjectRepository(em).removeByUserId(userId, id);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new ProjectRepository(em).removeAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    @NotNull
    public List<Project> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Project> projects = new ProjectRepository(em).findByName(userId, name);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Override
    @NotNull
    public List<Project> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Project> projects = new ProjectRepository(em).findByDescription(userId, description);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Project> projects = new ProjectRepository(em).sortByCreateTime(userId, direction);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Override
    public @NotNull List<Project> sortByStartDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Project> projects = new ProjectRepository(em).sortByStartDate(userId, direction);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Project> projects = new ProjectRepository(em).sortByFinishDate(userId, direction);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortByStatus(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Project> projects = new ProjectRepository(em).sortByStatus(userId, direction);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) return null;
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final Project project = new ProjectRepository(em).findOne(id);
        em.getTransaction().commit();
        em.close();
        return project;
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Project> projects = new ProjectRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        @NotNull final EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        new ProjectRepository(em).persist(project);
        transaction.commit();
        em.close();
    }

    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        @NotNull final EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        new ProjectRepository(em).merge(project);
        transaction.commit();
        em.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid project id");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        @NotNull final EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        @Nullable final Project project = findOne(id);
        if(project == null) return;
        new ProjectRepository(em).remove(project);
        transaction.commit();
        em.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new ProjectRepository(em).removeAll();
        em.getTransaction().commit();
        em.close();
    }
}
