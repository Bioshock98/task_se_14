package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.wbs.IAbstractVBSService;
import ru.pyshinskiy.tm.entity.AbstractWBS;

import java.util.List;

public abstract class AbstractWBSService<T extends AbstractWBS> extends AbstractService<T> implements IAbstractVBSService<T> {

    @Override
    @Nullable
    abstract public T findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Override
    @NotNull
    abstract public List<T> findAllByUserId(@Nullable final String userId) throws Exception;

    @Override
    abstract public void removeByUserId(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Override
    abstract public void removeAll(@Nullable final String userId) throws Exception;

    @Override
    @NotNull
    abstract public List<T> findByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    @Override
    @NotNull
    abstract public List<T> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception;

    @NotNull
    abstract public List<T> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception;

    @NotNull
    abstract public List<T> sortByStartDate(@Nullable final String userId, final int direction) throws Exception;

    @NotNull
    abstract public List<T> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception;

    @NotNull
    abstract public List<T> sortByStatus(@Nullable final String userId, final int direction) throws Exception;
}
