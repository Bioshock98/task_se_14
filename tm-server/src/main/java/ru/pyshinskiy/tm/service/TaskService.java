package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.util.List;

public final class TaskService extends AbstractWBSService<Task> implements ITaskService {

    @NotNull private final Bootstrap bootstrap;

    public TaskService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Task findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final Task task;
        try {
            task = new TaskRepository(em).findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllByUserId(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).findAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Override
    public void removeByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new TaskRepository(em).removeByUserId(userId, id);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new TaskRepository(em).removeAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    @NotNull
    public List<Task> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).findByName(userId, name);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).findByDescription(userId, description);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).sortByCreateTime(userId, direction);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Override
    public @NotNull List<Task> sortByStartDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).sortByStartDate(userId, direction);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).sortByFinishDate(userId, direction);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortByStatus(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).sortByStatus(userId, direction);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final Task task = new TaskRepository(em).findOne(id);
        em.getTransaction().commit();
        em.close();
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception("invalid task");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        @NotNull final EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        new TaskRepository(em).persist(task);
        transaction.commit();
        em.close();
    }

    @Override
    public void merge(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception("invalid task");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        @NotNull final EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        new TaskRepository(em).merge(task);
        transaction.commit();
        em.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid task id");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        @NotNull final EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        @Nullable final Task task = findOne(id);
        if(task == null) throw new Exception("task is null");
        new TaskRepository(em).remove(task);
        transaction.commit();
        em.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new TaskRepository(em).removeAll();
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String taskId) throws Exception {
        if(userId == null || taskId == null) throw new Exception("one of the parameters passed is zero");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<Task> tasks = new TaskRepository(em).findAllByProjectId(userId, taskId);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }
}
