package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final Bootstrap bootstrap;

    public UserService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final User user = new UserRepository(em).findOne(id);
        em.getTransaction().commit();
        em.close();
        return user;
    }

    @Override
    @NotNull
    public List<User> findAll() {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @NotNull final List<User> users = new UserRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return users;
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new UserRepository(em).persist(user);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void merge(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new UserRepository(em).merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final User user = findOne(id);
        if(user == null) return;
        new UserRepository(em).remove(user);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        new UserRepository(em).removeAll();
        em.getTransaction().commit();
        em.close();
    }

    @Override
    @Nullable
    public User getUserByLogin(@Nullable final String login) throws Exception {
        if(login == null) throw new Exception("login is invalid");
        @NotNull final EntityManager em = bootstrap.getEntityMeneger();
        em.getTransaction().begin();
        @Nullable final User user;
        try {
            user = new UserRepository(em).getUserByLogin(login);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return user;
    }
}
