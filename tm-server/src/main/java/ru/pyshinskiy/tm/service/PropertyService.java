package ru.pyshinskiy.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Getter
@Setter
public class PropertyService {

    @NotNull final String host;

    @NotNull final String password;

    @NotNull final String login;

    @NotNull final String driver;

    public PropertyService() {
        @NotNull
        final Properties properties = new Properties();
        @NotNull final InputStream is = Bootstrap.class.getResourceAsStream("/database.properties");
        try {
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.login = properties.getProperty("db.login");
        this.host = properties.getProperty("db.host");
        this.password = properties.getProperty("db.password");
        this.driver = properties.getProperty("jdbcDriver");
    }
}
