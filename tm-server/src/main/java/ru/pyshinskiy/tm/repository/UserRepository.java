package ru.pyshinskiy.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return em.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) {
        return em.find(User.class, id);
    }

    @Override
    public void persist(@NotNull final User user) {
        em.persist(user);
    }

    @Override
    public void merge(@NotNull final User user) {
        em.merge(user);
    }

    @Override
    public void remove(@NotNull final User user) {
        em.remove(user);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM User ").executeUpdate();
    }

    @Override
    @Nullable
    public User getUserByLogin(@NotNull final String login) {
        return em.createQuery("SELECT u FROM User u WHERE u.login = :login", User.class).setParameter("login", login).getSingleResult();
    }
}
