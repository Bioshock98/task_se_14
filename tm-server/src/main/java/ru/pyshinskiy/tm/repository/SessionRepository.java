package ru.pyshinskiy.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.session.ISessionRepository;
import ru.pyshinskiy.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        return em.createQuery("SELECT s FROM Session s", Session.class).getResultList();
    }

    @Override
    @NotNull
    public List<Session> findAllByUserId(@NotNull final String userId) {
        return em.createQuery("SELECT s FROM Session s WHERE s.user.id = :userId", Session.class).getResultList();
    }

    @Override
    @Nullable
    public Session findOne(@NotNull final String id) {
        return em.find(Session.class, id);
    }

    @Override
    public void persist(@NotNull final Session session) {
        em.persist(session);
    }

    @Override
    public void merge(@NotNull final Session session) {
        em.merge(session);
    }

    @Override
    public void remove(@NotNull final Session session) {
        em.remove(session);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Session").executeUpdate();
    }

    @Override
    @Nullable
    public Session findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return em.createQuery("SELECT s FROM Session s WHERE s.user.id= :userId AND s.id= :id", Session.class).setParameter("userId", userId).setParameter("id", id).getSingleResult();
    }

    @Override
    public void removeByUserId(@NotNull final String userId, @NotNull final String id) {
        em.createQuery("DELETE FROM Session s WHERE s.user.id = :userId AND s.id = :id").setParameter("userId", userId).setParameter("id", id).executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws Exception {
        em.createQuery("DELETE FROM Session s WHERE s.user.id = :userId").setParameter("userId", userId).executeUpdate();
    }
}
