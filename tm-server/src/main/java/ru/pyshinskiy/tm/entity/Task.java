package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.TaskDTO;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "app_task")
public final class Task extends AbstractWBS implements Serializable {

    @ManyToOne
    @Nullable
    private Project project;

    @Nullable
    public static TaskDTO toTaskDTO(@Nullable final Task task) {
        if(task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setCreateTime(task.getCreateTime());
        taskDTO.setUserId(task.getUser().getId());
        if(task.getProject() != null) {
            taskDTO.setProjectId(task.getProject().getId());
        }
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setStartDate(task.getStartDate());
        taskDTO.setFinishDate(task.getFinishDate());
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }

    @NotNull
    public static List<TaskDTO> toTasksDTO(@NotNull final List<Task> tasks) {
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for(Task task : tasks) {
            @NotNull final TaskDTO taskDTO = new TaskDTO();
            taskDTO.setId(task.getId());
            taskDTO.setCreateTime(task.getCreateTime());
            taskDTO.setUserId(task.getUser().getId());
            if(task.getProject() != null) {
                taskDTO.setProjectId(task.getProject().getId());
            }
            taskDTO.setName(task.getName());
            taskDTO.setDescription(task.getDescription());
            taskDTO.setStartDate(task.getStartDate());
            taskDTO.setFinishDate(task.getFinishDate());
            taskDTO.setStatus(task.getStatus());
            tasksDTO.add(taskDTO);
        }
        return tasksDTO;
    }
}
