package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@MappedSuperclass
public abstract class AbstractWBS extends AbstractEntity {

    @ManyToOne
    @NotNull
    private User user;

    @NotNull
    @Basic(optional = false)
    private Date createTime = new Date(System.currentTimeMillis());

    @Basic
    @Nullable
    private String name;

    @Basic
    @Nullable
    private String description;

    @Basic(optional = false)
    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Basic
    @Nullable
    private Date startDate;

    @Basic
    @Nullable
    private Date finishDate;
}
