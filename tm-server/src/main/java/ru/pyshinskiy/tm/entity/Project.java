package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.ProjectDTO;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "app_project")
public final class Project extends AbstractWBS {

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    public static ProjectDTO toProjectDTO(@Nullable final Project project) {
        if(project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setCreateTime(project.getCreateTime());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setStartDate(project.getStartDate());
        projectDTO.setFinishDate(project.getFinishDate());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

    @NotNull
    public static List<ProjectDTO> toProjectsDTO(@NotNull final List<Project> projects) {
        List<ProjectDTO> projectsDTO = new ArrayList<>();
        for(Project project : projects) {
            @NotNull final ProjectDTO projectDTO = new ProjectDTO();
            projectDTO.setId(project.getId());
            projectDTO.setCreateTime(project.getCreateTime());
            projectDTO.setUserId(project.getUser().getId());
            projectDTO.setName(project.getName());
            projectDTO.setDescription(project.getDescription());
            projectDTO.setStartDate(project.getStartDate());
            projectDTO.setFinishDate(project.getFinishDate());
            projectDTO.setStatus(project.getStatus());
            projectsDTO.add(projectDTO);
        }
        return projectsDTO;
    }
}