package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.util.security.PasswordHashUtil;
import ru.pyshinskiy.tm.util.security.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

import static ru.pyshinskiy.tm.constant.AppConst.CICLE;
import static ru.pyshinskiy.tm.constant.AppConst.SALT;

@NoArgsConstructor
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @WebMethod
    @Override
    @Nullable
    public UserDTO findOneUser(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        return User.toUserDTO(bootstrap.getUserService().findOne(id));
    }

    @WebMethod
    @Override
    @NotNull
    public List<UserDTO> findAllUsers(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        return User.toUsersDTO(bootstrap.getUserService().findAll());
    }

    @WebMethod
    @Override
    public void persistUser(@NotNull final UserDTO userDTO) throws Exception {
        userDTO.setPasswordHash(SignatureUtil.sign(userDTO.getPasswordHash(), SALT, CICLE));
        bootstrap.getUserService().persist(UserDTO.toUser(bootstrap, userDTO));
    }

    @WebMethod
    @Override
    public void mergeUser(@Nullable final SessionDTO sessionDTO, @Nullable final UserDTO userDTO) throws Exception {
        userDTO.setPasswordHash(PasswordHashUtil.md5(userDTO.getPasswordHash()));
        validateSession(sessionDTO);
        bootstrap.getUserService().merge(UserDTO.toUser(bootstrap, userDTO));
    }

    @WebMethod
    @Override
    public void removeUser(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getUserService().remove(id);
    }

    @WebMethod
    @Override
    public void removeAllUsers(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getUserService().removeAll();
    }

    @WebMethod
    @Override
    @Nullable
    public UserDTO getUserByLoginE(@Nullable final SessionDTO sessionDTO, @Nullable final String login) throws Exception {
        validateSession(sessionDTO);
        return User.toUserDTO(bootstrap.getUserService().getUserByLogin(login));
    }
}
