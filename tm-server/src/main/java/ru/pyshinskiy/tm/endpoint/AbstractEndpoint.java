package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.entity.Session;

import java.util.Date;

import static ru.pyshinskiy.tm.constant.AppConst.*;
import static ru.pyshinskiy.tm.util.security.SignatureUtil.sign;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    protected Bootstrap bootstrap;

    public AbstractEndpoint(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    protected void validateSession(@Nullable final SessionDTO userSession) throws Exception {
        if(userSession == null) throw new Exception("method is unavailable for unauthorized users");
        @Nullable final Session session = bootstrap.getSessionService().findOneByUserId(userSession.getUserId(), userSession.getId());
        if(session == null) throw new Exception("Session is doesn't exist");
        if(!session.getSignature().equals(sign(userSession, SALT, CICLE))) {
            throw new Exception("Session is invalid");
        }
        if(new Date().getTime() - userSession.getTimestamp().getTime() > SESSION_LIFE_TIME) {
            bootstrap.getSessionService().removeByUserId(userSession.getUserId(), userSession.getId());
            throw new Exception("Session time out\n you need to log in again");
        }
    }
}
