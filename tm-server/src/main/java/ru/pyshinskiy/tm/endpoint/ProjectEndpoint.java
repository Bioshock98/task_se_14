package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @WebMethod
    @Nullable
    @Override
    public ProjectDTO findOneProject(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectDTO(bootstrap.getProjectService().findOne(id));
    }

    @WebMethod
    @NotNull
    @Override
    public List<ProjectDTO> findAllProjects(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().findAll());
    }

    @WebMethod
    @Override
    public void persistProject(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().persist(ProjectDTO.toProject(bootstrap, projectDTO));
    }

    @WebMethod
    @Override
    public void mergeProject(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().merge(ProjectDTO.toProject(bootstrap, projectDTO));
    }

    @WebMethod
    @Override
    public void removeProject(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().remove(id);
    }

    @WebMethod
    @Override
    public void removeAllProjects(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().removeAll();
    }

    @WebMethod
    @Override
    @Nullable
    public ProjectDTO findOneProjectByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        @Nullable final ProjectDTO projectDTO =  Project.toProjectDTO(bootstrap.getProjectService().findOneByUserId(session.getUser().getId(), id));
        return projectDTO;
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> findAllProjectsByUserId(@Nullable final SessionDTO sessionDTO) throws Exception{
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().findAllByUserId(session.getUser().getId()));
    }

    @WebMethod
    @Override
    public void removeProjectByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().remove(id);
    }

    @WebMethod
    @Override
    public void removeAllProjectsByUserId(@Nullable final SessionDTO sessionDTO) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        bootstrap.getProjectService().removeAll(session.getUser().getId());
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> findProjectByName(@Nullable final SessionDTO sessionDTO, @Nullable final String name) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().findByName(session.getUser().getId(), name));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> findProjectByDescription(@Nullable final SessionDTO sessionDTO, @Nullable final String description) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().findByDescription(session.getUser().getId(), description));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> sortProjectsByCreateTime(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().sortByCreateTime(userId, direction));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> sortProjectsByStartDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().sortByStartDate(userId, direction));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> sortProjectsByFinishDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().sortByFinishDate(userId, direction));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> sortProjectsByStatus(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().sortByStatus(userId, direction));
    }
}
