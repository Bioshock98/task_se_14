package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.dto.TaskDTO;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @WebMethod
    @Nullable
    @Override
    public TaskDTO findOneTask(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        return Task.toTaskDTO(bootstrap.getTaskService().findOne(id));
    }

    @WebMethod
    @NotNull
    @Override
    public List<TaskDTO> findAllTasks(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().findAll());
    }

    @WebMethod
    @Override
    public void persistTask(@Nullable final SessionDTO sessionDTO, @Nullable final TaskDTO taskDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getTaskService().persist(TaskDTO.toTask(bootstrap, taskDTO));
    }

    @WebMethod
    @Override
    public void mergeTask(@Nullable final SessionDTO sessionDTO, @Nullable final TaskDTO taskDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getTaskService().merge(TaskDTO.toTask(bootstrap, taskDTO));
    }

    @WebMethod
    @Override
    public void removeTask(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getTaskService().remove(id);
    }

    @WebMethod
    @Override
    public void removeAllTasks(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getTaskService().removeAll();
    }

    @WebMethod
    @Override
    @Nullable
    public TaskDTO findOneTaskByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTaskDTO(bootstrap.getTaskService().findOneByUserId(session.getUser().getId(), id));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findAllTasksByUserId(@Nullable final SessionDTO sessionDTO) throws Exception{
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().findAllByUserId(session.getUser().getId()));
    }

    @WebMethod
    @Override
    public void removeTaskByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        bootstrap.getTaskService().removeByUserId(session.getUser().getId(), id);
    }

    @WebMethod
    @Override
    public void removeAllTasksByUserId(@Nullable final SessionDTO sessionDTO) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        bootstrap.getTaskService().removeAll(session.getUser().getId());
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findTasksByName(@Nullable final SessionDTO sessionDTO, @Nullable final String name) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().findByName(session.getUser().getId(), name));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findTasksByDescription(@Nullable final SessionDTO sessionDTO, @Nullable final String description) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().findByDescription(session.getUser().getId(), description));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> sortTasksByCreateTime(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().sortByCreateTime(userId, direction));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> sortTasksByStartDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().sortByStartDate(userId, direction));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> sortTasksByFinishDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().sortByFinishDate(userId, direction));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> sortTasksByStatus(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().sortByStatus(userId, direction));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findTasksByProjectId(@Nullable final SessionDTO sessionDTO, @Nullable final String projectId) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().findAllByProjectId(session.getUser().getId(), projectId));
    }
}
