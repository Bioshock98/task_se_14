package ru.pyshinskiy.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.endpoint.ProjectEndpoint;
import ru.pyshinskiy.tm.endpoint.SessionEndpoint;
import ru.pyshinskiy.tm.endpoint.TaskEndpoint;
import ru.pyshinskiy.tm.endpoint.UserEndpoint;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.service.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class Bootstrap {

    @NotNull private final IProjectService projectService = new ProjectService(this);

    @NotNull private final ITaskService taskService = new TaskService(this);

    @NotNull private final IUserService userService = new UserService(this);

    @NotNull private final ISessionService sessionService = new SessionService(this);

    @NotNull private final PropertyService propertyService = new PropertyService();

    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @Nullable private EntityManagerFactory entityManagerFactory;

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

    private void setEntityManagerFactory(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @NotNull
    public EntityManager getEntityMeneger() {
        return entityManagerFactory.createEntityManager();
    }

    private EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getHost());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getLogin());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, "update");
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, "true");
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    public void start() {
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskservice?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userservice?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionservice?wsdl", sessionEndpoint);
        setEntityManagerFactory(getEntityManagerFactory());
        @NotNull final SessionCleaner sessionCleaner = new SessionCleaner();
        sessionCleaner.setSessionService(sessionService);
        sessionCleaner.setDaemon(true);
        sessionCleaner.start();
    }
}
