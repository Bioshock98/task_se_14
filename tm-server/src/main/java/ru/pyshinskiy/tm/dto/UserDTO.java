package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

@NoArgsConstructor
@Getter
@Setter
public final class UserDTO extends AbstractEntityDTO {

    @NotNull
    private String login;


    @NotNull
    private String passwordHash;

    @NotNull
    private Role role;

    @NotNull
    public static User toUser(@NotNull final Bootstrap bootstrap, @NotNull final UserDTO userDTO) throws Exception{
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRole(userDTO.getRole());
        user.setProjects(bootstrap.getProjectService().findAllByUserId(userDTO.getId()));
        user.setTasks(bootstrap.getTaskService().findAllByUserId(userDTO.getId()));
        return user;
    }
}
