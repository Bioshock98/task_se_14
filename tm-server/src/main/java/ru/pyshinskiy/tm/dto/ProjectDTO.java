package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.Project;

@NoArgsConstructor
@Getter
@Setter
public final class ProjectDTO extends AbstractWBSDTO {

    @NotNull
    public static Project toProject(@NotNull final Bootstrap bootstrap, @NotNull final ProjectDTO projectDTO) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setUser(bootstrap.getUserService().findOne(projectDTO.getUserId()));
        project.setCreateTime(projectDTO.getCreateTime());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStartDate(projectDTO.getStartDate());
        project.setFinishDate(projectDTO.getFinishDate());
        project.setStatus(projectDTO.getStatus());
        project.setTasks(bootstrap.getTaskService().findAllByProjectId(projectDTO.getUserId(), projectDTO.getId()));
        return project;
    }

   /* @Override
    public boolean equals(@NotNull final Object object) {
        if(!(object instanceof ProjectDTO)) return false;
        @NotNull final ProjectDTO projectDTO = (ProjectDTO) object;
        if(!this.getId().equals(projectDTO.getId())) return false;
        if(!this.getUserId().equals(projectDTO.getUserId())) return false;
        if(!this.getName().equals(projectDTO.getName())) return false;
        if(!this.getDescription().equals(projectDTO.getDescription())) return false;
        if(!this.getCreateTime().equals(projectDTO.getCreateTime())) return false;
        if(!this.getStatus().equals(projectDTO.getStatus())) return false;
        if(!this.getStartDate().equals(projectDTO.getStartDate())) return false;
        if(!this.getFinishDate().equals(projectDTO.getFinishDate())) return false;
        return true;
    }*/
}
