package ru.pyshinskiy.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("USER"),
    ADMINISTRATOR("ADMINISTRATOR"),
    TEST("TEST");

    @NotNull private String name;

    Role(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
